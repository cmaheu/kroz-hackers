// kroz.cpp : Defines the entry point for the console application.
//

#include <string>
#include <iostream>
#include <unordered_map>
#include <algorithm>
#include <sstream>
#include <functional> 
#include <cctype>
#include <locale>
#include <stdlib.h>
#include <time.h>

using namespace std;

enum Choice { n, s, e, w, c, l, t, u, q, h, i, b };
unordered_map < string, Choice > ChoiceMap = {
	{ "n", n }, { "north", n },
	{ "s", s }, { "south", s },
	{ "e", e }, { "east", e },
	{ "w", w }, { "west", w },
	{ "c", c }, { "climb", c },
	{ "l", l }, { "look", l },
	{ "t", t }, { "take", t },
	{ "u", u }, { "use", u }, 
	{ "q", q }, { "quit", q },
	{ "h", h }, { "help", h },
	{ "i", i }, { "inventory", i }, { "items", i },
	{ "peter bui", b }, { "bui", b }
};


// trim from start
string ltrim(std::string s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(),
		std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim from end
string rtrim(std::string s) {
	s.erase(std::find_if(s.rbegin(), s.rend(),
		std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

// trim from both ends
string trim(std::string s) {
	return ltrim(rtrim(s));
}

string getmyline()
{
	string line;
	getline(cin, line);

	transform(line.begin(), line.end(), line.begin(), ::tolower);
	return line;
}

void clearscreen()
{
#ifdef _WIN32
	system("cls");
#elif _WIN64
	system("cls");
#else
	system("clear");
#endif

}

class Item {
public:
	string name;
	string description;
	int state;
	Item(string _name, string _description)
	{
		name = _name;
		description = _description;
		state = 0;
	}
	bool operator==(const Item &other) const {
		return name == other.name && state == other.state;
	}

};

class Location;

class GameState {
	Location * playerLoc;
public:
	Location * getPlayerLoc() { return playerLoc; }
	void setPlayerLoc(Location * newLoc) { playerLoc = newLoc; }
	vector<Item> items;
	int board[9] = {0,0,0,0,0,0,0,0,2};
	string arg;

	// Game Variables
	bool switchTriggered;
	int ropePos; // 0: hanging, 1: broken, 2: taken. Used in Pit
	bool ropeOnRock; // Used in DropOff
	bool takenLantern; // Used in CliffBottom

	void reset() {
		items.clear();
		setPlayerLoc(NULL);
		arg = "";
		switchTriggered = false;
		ropePos = 0;
		ropeOnRock = false;
		takenLantern = false;
		for (int i = 0; i<9; i++){
			board[i] = 0;
		}
		board[8]=2;
	}
	void removeItem(string item)
	{
		items.erase(find(items.begin(), items.end(), Item(item,"")));
	}
	void addItem(string name, string description)
	{
		items.push_back(Item(name, description));
	}
	bool hasItem(string item)
	{
		return find(items.begin(), items.end(), Item(item, "")) != items.end();
	}
};

class Location {
	string text;
public:
	Location * north = NULL;
	string nm;
	Location * south = NULL;
	string sm;
	Location * east = NULL;
	string em;
	Location * west = NULL;
	string wm;
	Location(string _text = "", string _nm = "", string _sm = "", string _em = "", string _wm = "")
	{
		text = _text;
		nm = _nm;
		sm = _sm;
		wm = _wm;
		em = _em;
	}
	void help()
	{
		cout << "Commands: " << endl;
		cout << "(N)orth, (S)outh, (E)ast, (W)est" << endl;
		cout << "(L)ook, (H)elp, (Q)uit, (I)nventory" << endl;
		cout << "(T)ake (item), (U)se (item), (C)limb" << endl;
	}
	void inventory(GameState gs)
	{
		if (gs.items.size() > 0)
			for (auto &item : gs.items)
				cout << "You have " << item.description << endl;
		else cout << "No items in inventory" << endl;
	}
	virtual GameState moven(GameState gs)
	{
		if (north) { north->look(gs); gs.setPlayerLoc(north); }
		else cout << nm << endl;
		return gs;
	}
	virtual GameState moves(GameState gs)
	{
		if (south) { south->look(gs); gs.setPlayerLoc(south); }
		else cout << sm << endl;
		return gs;
	}
	virtual GameState movee(GameState gs)
	{
		if (east) { east->look(gs); gs.setPlayerLoc(east); }
		else cout << em << endl;
		return gs;
	}
	virtual GameState movew(GameState gs)
	{
		if (west) { west->look(gs); gs.setPlayerLoc(west); }
		else cout << wm << endl;
		return gs;
	}
	virtual GameState climb(GameState gs) {
		cout << "There is nothing to climb." << endl;
		return gs;
	}
	virtual GameState take(GameState gs) {
		cout << "There is nothing to take." << endl;
		return gs;
	}
	virtual GameState takerope(GameState gs) {
		cout << "There is nothing to take." << endl;
		return gs;
	}
	virtual GameState takelantern(GameState gs) {
		cout << "There is nothing to take." << endl;
		return gs;
	}
	virtual GameState use(GameState gs) {
		cout << "There is nothing to use." << endl;
		return gs;
	}
	virtual void look(GameState gs) {
		cout << text << endl;
	}
	virtual GameState bui(GameState gs) {
		cout << "I am not sure what you mean." << endl;
		return gs;
	}
	virtual void reset(){}
};

GameState takeAction(GameState gs, Choice choice, Location * endGame)
{
	switch (choice)
	{
	case n: gs = gs.getPlayerLoc()->moven(gs); break;
	case s: gs = gs.getPlayerLoc()->moves(gs); break;
	case e: gs = gs.getPlayerLoc()->movee(gs); break;
	case w: gs = gs.getPlayerLoc()->movew(gs); break;
	case c: gs = gs.getPlayerLoc()->climb(gs); break;
	case t: gs = gs.getPlayerLoc()->take(gs); break;
	case u: gs = gs.getPlayerLoc()->use(gs); break;
	case b: gs = gs.getPlayerLoc()->bui(gs); break;
	case h: gs.getPlayerLoc()->help(); break;
	case i: gs.getPlayerLoc()->inventory(gs); break;
	case l: gs.getPlayerLoc()->look(gs); break;
	case q: gs.setPlayerLoc(endGame); break;
	}
	return gs;
}


class Pit : public Location {
	GameState climb(GameState gs)
	{
		if (gs.ropePos > 0) {
			cout << "There is nothing to climb" << endl;
		}
		else {
			string instr;
			while (1)
			{
				cout << "The rope is fraying and you are not sure it will support your weight. Are you sure you want to attempt to climb it ?" << endl;
				instr = getmyline();
				clearscreen();
				transform(instr.begin(), instr.end(), instr.begin(), ::tolower);
				if (instr == "y" || instr == "yes")
				{
					cout << "You begin to climb up the rope, but when you are halfway up, the rope breaks and you fall back into the pit.Luckily you are able to avoid injury." << endl;
					gs.ropePos = 1;
					break;
				}
				else if (instr == "n" || instr == "no") break;
				else cout << "I'm not sure what you meant. Type yes or no." << endl;
			}
		}
		return gs;
	}
	void look(GameState gs)
	{
		switch (gs.ropePos)
		{
		case 0: cout << "You are in the center of a pit. A rope is hanging down into the center of the pit. There is a small tunnel to the south." << endl; break;
		case 1: cout << "You are in the center of a pit. A rope is on the ground in the center of the pit. There is a small tunnel to the south." << endl; break;
		case 2: cout << "You are in the center of a pit. There is a small tunnel to the south." << endl; break;
		}
	}
	GameState take(GameState gs)
	{
		if (gs.arg == "rope" || gs.arg == "")
			switch (gs.ropePos)
			{
			case 0: cout << "The rope is tied up above, so you cannot take it. You might be able to climb it though." << endl; break;
			case 1: cout << "You take the broken piece of rope from the ground." << endl; 
				gs.ropePos = 2; 
				gs.addItem("rope", "a long rope that is frayed at one end. ");
				return gs;
				break;
			case 2: cout << "There is nothing to take." << endl; break;
			}
		else cout << "You cannot take that." << endl;
		return gs;
	}
};

class PitWall : public Location {
	GameState climb(GameState gs)
	{
		cout << "You attempt to climb the pit walls, but the crack is not wide enough to create handholds." << endl;
		return gs;
	}
	void look(GameState gs)
	{
		cout << "You are at the wall of the pit. A crack appears to zig-zag around and up the pit." << endl;
	}
};

class DropOff : public Location {
public:
	Location * cliffBottom;
	Location * endGame;
	GameState movew(GameState gs)
	{
		string instr;
		while (1)
		{
			cout << "This looks like a long fall, are you sure you want to go this way?" << endl;
			instr = getmyline();
			transform(instr.begin(), instr.end(), instr.begin(), ::tolower);
			if (instr == "y" || instr == "yes")
			{
				cout << "You bravely step forward off the edge of the drop off and fall to your death. Game Over!" << endl;
				gs.setPlayerLoc (endGame);
				return gs;
				break;
			}
			else if (instr == "n" || instr == "no") break;
			else cout << "I'm not sure what you meant. Type yes or no." << endl;
		}
		return gs;
	}
	GameState climb(GameState gs)
	{
		if (!gs.ropeOnRock)
		{
			cout << "The cliff is too sheer. You cannot find a way to climb down it easily." << endl;
			return gs;
		}
		else {
			cout << "You climb down the rope until you reach the bottom." << endl;
			cliffBottom->look(gs);
			gs.setPlayerLoc(cliffBottom);
			return gs;
		}
	}
	void look(GameState gs)
	{
		if (!gs.ropeOnRock)
			cout << "You follow the tunnel west for a while. The tunnel slowly gets a little lighter, and you are just able to see a drop off before you walk right off of it." << endl;
		else cout << "You are standing at a drop off, with a rope tied to a large rock and hanging down." << endl;
	}
	GameState use(GameState gs)
	{
		if ((gs.arg == "rope" || gs.arg == "") && !gs.ropeOnRock && gs.hasItem("rope"))
		{
			cout << "You tie the edge of the rope around a large rock." << endl;
			gs.ropeOnRock = true;
			gs.removeItem("rope");
			return gs;
		}
		if (gs.arg == "") cout << "You have nothing to use" << endl;
		else  cout << "You cannot use that." << endl;
		return gs;
	}
};

class CliffBottom : public Location {
public:
	Location * cliffTop;
	GameState climb(GameState gs)
	{
		cout << "You climb up the rope until you reach the top." << endl;
		cliffTop->look(gs);
		gs.setPlayerLoc(cliffTop);
		return gs;
	}
	void look(GameState gs)
	{
		if (!gs.takenLantern)
			cout << "You are in a small room at the bottom of a large drop off. A rope leads back up the cliff.There is a bag lying on the ground." << endl;
		else cout << "You are in a small room at the bottom of a large drop off. A rope leads back up the cliff." << endl;
	}
	GameState take(GameState gs)
	{
		if ((gs.arg == "bag" || gs.arg == "") && !gs.takenLantern)
		{
			cout << "You take the bag. Inside are several wooden X's." << endl;
			gs.addItem("bag", "a bag that contains wooden X's.");
			gs.takenLantern = true;
			return gs;
		}
		if (gs.arg == "") cout << "There is nothing to take" << endl;
		else  cout << "You cannot take that." << endl;
		return gs;
	}
};

class DarkTunnel : public Location {
	string darktext;
	string lighttext;
public:
	DarkTunnel(string dt, string lt)
	{
		darktext = dt;
		lighttext = lt;
	}
	void look(GameState gs)
	{
		if (gs.hasItem("lantern")) cout << lighttext << endl;
		else cout << darktext << endl;
	}
};

class DarkRoom : public Location{
	string darktext;
	string lighttext;
public:
	DarkRoom(string dt, string lt)
	{
		darktext = dt;
		lighttext = lt;
	}
	void look(GameState gs)
	{
		if (gs.hasItem("lantern")) cout << lighttext << endl;
		else cout << darktext << endl;
	}
	GameState bui(GameState gs) {
		if (gs.hasItem("lantern"))
		{
			cout << "You hear a rumbling from down the tunnel." << endl;
			gs.switchTriggered = true;
			return gs;
		}
		else
		{
			cout << "What are you doing you cheater?" << endl;
			return gs;
		}
	}
};

class TunnelEntrance : public Location {
public:
	Location * newnorth;
	GameState moven(GameState gs)
	{
		if (gs.switchTriggered) { newnorth->look(gs); gs.setPlayerLoc(newnorth); }
		else { north->look(gs); gs.setPlayerLoc(north);}
		return gs;
	}
	void look(GameState gs)
	{
		if (gs.switchTriggered)
			cout << "You are at the entrance of a dark tunnel leading out of the pit. Towards the north, a staircase begins that spirals upward around the pit." << endl;
		else
			cout << "You are at the entrance of a dark tunnel leading out of the pit. High above, a crack appears to zig - zag around and up the pit." << endl;

	}
};

class NewPit : public Location {
public:
	Location * stairsTop;
	GameState climb(GameState gs)
	{
		cout << "You climb up the stairs until you reach the top." << endl;
		stairsTop->look(gs);
		gs.setPlayerLoc(stairsTop);
		return gs;
	}
	void look(GameState gs) {
		cout << "You are back at the center of the pit, but now there is a staircase gound around the walls to the top." << endl;
	}
	
	
};

class TicTacToe : public Location {
	int gridNum;
public:
	TicTacToe(int i){
		gridNum = i;
	}
	void look(GameState gs)
	{
		cout << "You are in a square room, you see exits to the ";
		string lookStatement;
		switch (gridNum){
			case 1:
				cout << "west and south.";
				break;
			case 2:
				cout << "north, west, and south.";
				break;
			case 3:
				cout << "north and west.";
				break;
			case 4:
				cout << "west, east, and south.";
				break;
			case 5:
				cout << "north, west, east, and south.";
				break;
			case 6:
				cout << "north, west, and east.";
				break;
			case 7:
				cout << "east and south.";
				break;
			case 8:
				cout << "north, west, east, and south.";
				break;
			case 9:
				cout << "north and east.";
				break;
		}
		switch(gs.board[gridNum-1]){
			case 0:
				cout << endl;
				break;
			case 1:
				cout << " There is an X in the middle of the room." << endl;
				break;
			case 2:
				cout << " There is an O in the middle of the room." << endl;
				break;
		}
	}
	GameState use(GameState gs)
	{
		if ((gs.arg == "bag" || gs.arg == "") && gs.hasItem("bag"))
		{
			if (gs.board[gridNum-1] == 0){
				cout << "You take an X from the bag and place it in the center of the room." << endl;
				gs.board[gridNum-1] = 1;
				if (hasWon(gs)){
					cout << "You hear a rumbling from down the tunnel." << endl;
					gs.switchTriggered = true;
					return gs;
				}
				gs = placeO(gs);
			}
			else{
				cout << "You feel like you shouldn't place an X in a room that already has something in it." << endl;
			}
			return gs;
		}
		if (gs.arg == "") cout << "You have nothing to use" << endl;
		else  cout << "You cannot use that." << endl;
		return gs;
	}
	bool hasWon(GameState gs){
		if (gs.board[0]==1 && gs.board[1]==1 && gs.board[2]==1){
			return true;
		}
		if (gs.board[3]==1 && gs.board[4]==1 && gs.board[6]==1){
			return true;
		}
		if (gs.board[6]==1 && gs.board[7]==1 && gs.board[8]==1){
			return true;
		}
		if (gs.board[0]==1 && gs.board[3]==1 && gs.board[6]==1){
			return true;
		}
		if (gs.board[1]==1 && gs.board[4]==1 && gs.board[7]==1){
			return true;
		}
		if (gs.board[2]==1 && gs.board[5]==1 && gs.board[8]==1){
			return true;
		}
		if (gs.board[0]==1 && gs.board[4]==1 && gs.board[8]==1){
			return true;
		}
		if (gs.board[2]==1 && gs.board[4]==1 && gs.board[6]==1){
			return true;
		}
		return false;
	}
	GameState placeO(GameState gs){
		int boardSpot = rand() % 9;
		if (gs.board[boardSpot] == 0){
			gs.board[boardSpot] = 2;
			for (int i = 0; i <9; i++) {
				if (gs.board[i] == 0){
					return gs;
				}
			}
			cout << "The wooden X you just put down suddenly disappeared into thin air." << endl;
			for (int i = 0; i <9; i++) {
				gs.board[i] = 0;
			}
			gs.board[8] = 2;
			return gs;
		}
		else{
			gs = placeO(gs);
			return gs;
		}
	}
};



int main(int argc, char* argv[])
{
	srand(time(NULL));
	string instr,comm,arg;
	GameState gs;
	cout << "WELCOME TO KROZ!" << endl << endl;
	cout << "You awake at the bottom of a deep pit with no memory of how you ended up here. A rope is hanging down into the center of the pit. There is a small tunnel to the south." << endl;

	// Build Map
	Pit pit;
	gs.setPlayerLoc(&pit);
	PitWall pitwallwest;
	PitWall pitwalleast;
	PitWall pitwallnorth;
	TunnelEntrance tunnelentrance;
	DarkRoom tunnel("You are in a dark tunnel. There is some light coming in from the north The tunnel continues south.", "You are in a dark tunnel. There is some light coming in from the north The tunnel continues south. With the light of the lantern, you see \"ROT13\" scrawled on the wall.");
	Location fork("You are at a fork in the tunnel. There are paths to the north, east, and west.");
	DropOff dropoff;
	CliffBottom cliffBottom;
	Location endGame("The game is over. You should not see this message.");
	Location wonGame("");
	TicTacToe tictactoe1(1);
	TicTacToe tictactoe2(2);
	TicTacToe tictactoe3(3);
	TicTacToe tictactoe4(4);
	TicTacToe tictactoe5(5);
	TicTacToe tictactoe6(6);
	TicTacToe tictactoe7(7);
	TicTacToe tictactoe8(8);
	TicTacToe tictactoe9(9);
	DarkRoom largeRoom("You enter a large, dark room. You cannot see very much.", "With the light of your lantern, you can see \"Jub vf gur terngrfg pbzchgre fpvrapr cebsrffbe?\" scrawled on the wall.");
	NewPit newpit;

	pit.north = &pitwallnorth;
	pit.east = &pitwalleast;
	pit.west = &pitwallwest;
	pit.south = &tunnelentrance;
	pitwallnorth.nm = "There is a wall to the north that you cannot pass.";
	pitwallnorth.east = &pitwalleast;
	pitwallnorth.west = &pitwallwest;
	pitwallnorth.south = &pit;
	pitwalleast.north = &pitwallnorth;
	pitwalleast.em = "There is a wall to the east that you cannot pass.";
	pitwalleast.west = &pit;
	pitwalleast.south = &tunnelentrance;
	pitwallwest.north = &pitwallnorth;
	pitwallwest.east = &pit;
	pitwallwest.wm = "There is a wall to the west that you cannot pass.";
	pitwallwest.south = &tunnelentrance;
	tunnelentrance.north = &pit;
	tunnelentrance.east = &pitwalleast;
	tunnelentrance.west = &pitwallwest;
	tunnelentrance.south = &tunnel;
	tunnelentrance.newnorth = &newpit;
	tunnel.north = &tunnelentrance;
	tunnel.em = "There is no way to go east in this part of the tunnel.";
	tunnel.wm = "There is no way to go west in this part of the tunnel.";
	tunnel.south = &fork;
	fork.north = &tunnel;
	fork.east = &tictactoe8;
	fork.west = &dropoff;
	fork.sm = "There is no way to go south in this part of the tunnel.";
	tictactoe1.nm = "There is a wall to the north that you cannot pass.";
	tictactoe1.south = &tictactoe2;
	tictactoe1.em = "There is a wall to the east that you cannot pass.";
	tictactoe1.west = &tictactoe4;
	tictactoe2.em = "There is a wall to the east that you cannot pass.";
	tictactoe2.south = &tictactoe3;
	tictactoe2.north = &tictactoe1;
	tictactoe2.west = &tictactoe5;
	tictactoe3.sm = "There is a wall to the south that you cannot pass.";
	tictactoe3.em = "There is a wall to the east that you cannot pass.";
	tictactoe3.north = &tictactoe2;
	tictactoe3.west = &tictactoe6;
	tictactoe4.east = &tictactoe1;
	tictactoe4.south = &tictactoe5;
	tictactoe4.nm = "There is a wall to the north that you cannot pass.";
	tictactoe4.west = &tictactoe7;
	tictactoe5.east = &tictactoe2;
	tictactoe5.south = &tictactoe6;
	tictactoe5.north = &tictactoe4;
	tictactoe5.west = &tictactoe8;
	tictactoe6.east = &tictactoe3;
	tictactoe6.sm = "There is a wall to the south that you cannot pass.";
	tictactoe6.north = &tictactoe5;
	tictactoe6.west = &tictactoe9;
	tictactoe7.east = &tictactoe4;
	tictactoe7.wm = "There is a wall to the west that you cannot pass.";
	tictactoe7.south = &tictactoe8;
	tictactoe7.nm = "There is a wall to the north that you cannot pass.";
	tictactoe8.east = &tictactoe5;
	tictactoe8.north = &tictactoe7;
	tictactoe8.south = &tictactoe9;
	tictactoe8.west = &fork;
	tictactoe9.east = &tictactoe6;
	tictactoe9.north = &tictactoe8;
	tictactoe9.wm = "There is a wall to the west that you cannot pass.";
	tictactoe9.sm = "There is a wall to the north that you cannot pass.";
	dropoff.nm = "There is no way to go north in this part of the tunnel.";
	dropoff.sm = "There is no way to go south in this part of the tunnel.";
	dropoff.east = &fork;
	dropoff.cliffBottom = &cliffBottom;
	dropoff.endGame = &endGame;
	cliffBottom.nm = "There is no way to go north in this part of the tunnel.";
	cliffBottom.em = "There is no way to go east in this part of the tunnel.";
	cliffBottom.wm = "There is no way to go west in this part of the tunnel.";
	cliffBottom.sm = "There is no way to go south in this part of the tunnel.";
	cliffBottom.cliffTop = &dropoff;
	largeRoom.nm = "There is no way to go north in this part of the tunnel.";
	largeRoom.em = "There is no way to go east in this part of the tunnel.";
	largeRoom.west = &fork;
	largeRoom.sm = "There is no way to go south in this part of the tunnel.";
	newpit.south = &tunnelentrance;
	newpit.nm = "There is a staircase you can climb, but the wall of the pit prevents you from moving farther north.";
	newpit.em = "There is a staircase you can climb, but the wall of the pit prevents you from moving farther east.";
	newpit.wm = "There is a staircase you can climb, but the wall of the pit prevents you from moving farther west.";
	newpit.stairsTop = &wonGame;

	while (1) {
		gs.reset();
		tictactoe1.reset();
	 	tictactoe2.reset();
	 	tictactoe3.reset();
		tictactoe4.reset();
		tictactoe5.reset();
		tictactoe6.reset();
		tictactoe7.reset();
		tictactoe8.reset();
		tictactoe9.reset();
		gs.setPlayerLoc(&pit);
		while (1)
		{
			if (gs.getPlayerLoc() == &endGame) break;
			if (gs.getPlayerLoc() == &wonGame)
			{
				cout << "Congratulations! You have beaten Kroz!" << endl;
				break;
			}
			cout << endl;
			cout << "What would you like to do?" << endl;
			instr = getmyline() + " ";
			comm = trim(instr.substr(0, instr.find_first_of(" ")));
			gs.arg = trim(instr.substr(instr.find_first_of(" "), instr.size()));
			clearscreen();
			cout << endl;
			unordered_map < string, Choice >::iterator it = ChoiceMap.find(comm);
			if (it != ChoiceMap.end()) gs = takeAction(gs, ChoiceMap[comm],&endGame);
			else cout << "I am not sure what you mean." << endl;
		}
		cout << endl << "(R)estart or (Q)uit?" << endl;
		string c = getmyline();
		if (c != "r" && c != "restart")
			return 0;
	}
}
